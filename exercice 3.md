Exponiendo el servicio hacia el exterior (crea service1.yaml)

apiVersion: v1
kind: Service
metadata:
    name:my-Service
spec:
    type: NodePort
    selector:
        app: Myapp
    ports:
        port:80
        targetPort:80
        NodePort:30007

De forma interna, sin acceso desde el exterior (crea service2.yaml)

apiVersion: v1
kind: Service
metadata:
    name:my-Service
spec:
    selector:
        app: Myapp
    ports:
        port:80
        targetPort:80