apiVersion: autoscaling/v1
kind: HorizontalPodAutoscaler
metadata:
  name: nginx-doployment
spec:
  scaleTargetRef:
    kind: Deployment
    name: nginx-doployment
  minReplicas: 3
  maxReplicas: 3
  targetCPUUtilizationPercentage: 50