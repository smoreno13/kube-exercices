apiVersion: v1
kind: Service
metadata:
  name: nginx-service
  labels:
    name: nginx-service
spec:
  selector:
    app: nginx-server
    version: "1.19.4"
  type: NodePort
  ports:
    - name: http
      protocol: TCP
      port: 80
      nodePort: 30007