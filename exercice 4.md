Despliega una nueva versión de tu nuevo servicio mediante la técnica “recreate”

apiVersion : apps/v1
kind: Deployment
metadata:
    name: nginx-Deployment
    labels:
        app: nginx
spec:
    replicas: 3
    strategy:
        type: Recreate
    selector:
        matchLabels:
            app:nginx
    template:
        metadata:
            labels:
                app:nginx
        spec:
            containers:
                name:nginx
                image: nginx:1.19.4-alpine
                ports:
                    containerPort: 80


Despliega una nueva versión haciendo “rollout deployment”

-kubectl set image deployment/nombre de tu deployment nginx=nginx:1.16.1 --record

Realiza un rollback a la versión generada previamente

-kubectl rollout undo deployment/nombredeployment