apiVersion: apps/v1
kind: ReplicaSet
metadata:
  name: frontend
  labels:
    app: guestbook
spec:
  replicas: 3
  selector:
    matchLabels:
  template:
    metadata:
      labels:
    spec:
      containers:
      - name: nginx
        image: nginx 1.19.4 alpine



¿Cúal sería el comando que utilizarías para escalar el número de replicas a 10?

-kubectl scale --replicas=10 rs/nginx-server

Si necesito tener una replica en cada uno de los nodos de Kubernetes, ¿qué objeto se adaptaría mejor? (No es necesario adjuntar el objeto)

Podriamos tener un DaemonSet