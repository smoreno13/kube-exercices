apiVersion: apps/v1
kind: ReplicaSet
metadata:
  name: nginx-replicaset
spec:
  replicas: 3
  selector:
    matchLabels:
      app: nginx-server
      version: "1.19.4"
  template:
    metadata:
      labels:
        app: nginx-server
        version: "1.19.4"
    spec:
      containers:
        - name: nginx-pod
          image: nginx:1.19.4
          resources:
            limits:
              cpu: 20m
              memory: 128Mi
            requests:
              cpu: 20m
              memory: 128Mi
          ports:
            - containerPort: 80