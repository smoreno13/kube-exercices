apiVersion: v1
kind: Pod
metadata:
	name: nginx-server
	labels:
		app:nginx-server
spec:
	containers:
		name:nginx-container
		image: nginx:1.19.4
		ports:
			containerPort:80
		resources:
			limits:
				memory: "256Mi"
				cpu: 100m
			requests:
				memory: "256Mi"
				cpu: 100m


¿Cómo puedo obtener las últimas 10 líneas de la salida estándar (logs generados por la aplicación)?

-kubectl logs --tail=10 [nombre pod]

¿Cómo podría obtener la IP interna del pod? Aporta capturas para indicar el proceso que seguirías.

-kubectl get pod -o wide

¿Qué comando utilizarías para entrar dentro del pod?

-kubectl exec --stdin --tty [nombre pod] -- /bin/bash

Necesitas visualizar el contenido que expone NGINX, ¿qué acciones debes llevar a cabo?

-cat /usr/share/nginx/html/index.html

Indica la calidad de servicio (QoS) establecida en el pod que acabas de crear. ¿Qué lo has mirado?

-crear name space: kubectl create namespace qos-example
kubectl get pod qos-demo --namespace=qos-example --output=yaml
buscar lina qosclass:guaranteed
