blue.yaml:

apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
  labels:
    app: nginx
spec:
  replicas: 2
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
        version: "1.19.4"
    spec:
      containers:
      - name: nginx
        image: nginx:1.19.4
        ports:
        - containerPort: 80

green.yaml:

apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment1.11
  labels:
    app: nginx
spec:
  replicas: 2
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
        version: "1.11"
    spec:
      containers:
      - name: nginx
        image: nginx:1.11
        ports:
        - containerPort: 80

serviceBlue.yaml:

apiVersion: v1
kind: Service
metadata: 
  name: nginx
  labels: 
    name: nginx
spec:
  ports:
    - name: http
      port: 80
      targetPort: 80
  selector: 
    name: nginx
    version: "1.19.4"
  type: LoadBalancer

serviceGreen.yaml:

  apiVersion: v1
kind: Service
metadata: 
  name: nginx
  labels: 
    name: nginx
spec:
  ports:
    - name: http
      port: 80
      targetPort: 80
  selector: 
    name: nginx
    version: "1.11"
  type: LoadBalancer

comandos:

1- kubectl apply -f blue.yaml
2- kubectl apply -f serviceBlue.yaml
3- kubectl apply -f green.yaml
4- kubectl apply -f serviceGreen.yaml